<?php
$username = $_GET["username"];
$hash = $_GET["hash"];

if(checkHash($hash, [$username])) {
    if($stmt = $mysqli->prepare("SELECT * FROM `usernames` WHERE `username` = ?")) {
        $stmt->bind_param("s", $username);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $result = $stmt->get_result();
        $usernameResult = ($result->num_rows === 0);
        $stmt->close();

        if($usernameResult) {
            if($stmt = $mysqli->prepare("INSERT INTO `usernames` (`username`) VALUES (?)")) {
                $stmt->bind_param("s", $username);
                if (!$stmt->execute()) {
                    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                }
                print(json_encode([
                    "success" => "usernameCreated",
                    "username" => $username,
                ]));
                $stmt->close();
            } else {
                die("Error: ".$mysqli->error);
            }
        } else {
            print(json_encode([
                "error" => "usernameExists"
            ]));
        }
    } else {
        die("Error: ".$mysqli->error);
    }
}