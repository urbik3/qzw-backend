<?php
$username = $_GET["username"];
$score = $_GET["score"];
$chapter = $_GET["chapter"];
$character = $_GET["character"];
$hash = $_GET["hash"];

if(checkHash($hash, [$score, $username, $chapter, $character])) {
    if($stmt = $mysqli->prepare("SELECT score FROM `leaderboards` WHERE `chapter` = ? AND `character` = ? AND `username` = ?")) {
        $stmt->bind_param("sss", $chapter, $character, $username);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $result = $stmt->get_result();
        $arr = $result->fetch_array();
        $stmt->close();
    }

    $isScoreBetter = true;
    if($arr["score"]) {
        if($chapter === "chapterEndless") {
            $isScoreBetter = (int) $score > $arr["score"];
        } else {
            $isScoreBetter = (int) $score < $arr["score"];
        }
    }
    if($isScoreBetter) {
        if($stmt = $mysqli->prepare("REPLACE `leaderboards` (`username`, `score`, `chapter`, `character`) VALUES (?, ?, ?, ?)")) {
            $stmt->bind_param("ssss", $username, $score, $chapter, $character);
            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
            $stmt->close();
            print json_encode(["success" => "scoreInserted"]);
        }
    } else {
        print json_encode(["warning" => "scoreSucks"]);
    }
}