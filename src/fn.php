<?php

define("UID", '_F4rz7-e*04te47y$dFDDdf/78');

function checkHash($hash, $values) {
    $str = "";
    foreach($values as $value) {
        $str .= $value;
    }
    $hashResult = sha1(UID.$str) === $hash;

    return $hashResult;
}