<?php

$chapter = $_GET["chapter"];
$character = $_GET["character"];

$orderby = "ASC";
if($chapter == "chapterEndless") {
    $orderby = "DESC";
}
if($stmt = $mysqli->prepare("SELECT * FROM `leaderboards` WHERE `chapter` = ? AND `character` = ? ORDER BY score ".$orderby)) {
    $stmt->bind_param("ss", $chapter, $character);
    if (!$stmt->execute()) {
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    $result = $stmt->get_result();
    $ass_arr = [];
    while($arr = $result->fetch_array()) {
        $ass_arr[] = $arr;
    }
    print json_encode($ass_arr);
    $stmt->close();
} else {
    die("Error: ".$mysqli->error);
}