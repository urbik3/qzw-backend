<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$data = require "config.php";

$mysqli = new mysqli($data["hostname"], $data["username"], $data["password"], $data["database"]);
if($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

require "src/fn.php";

if(isset($_GET["getLeaderboard"]) and isset($_GET["chapter"]) and isset($_GET["character"])) {
    require "src/getLeaderboard.php";
}
elseif(isset($_GET["postScore"]) and isset($_GET["username"]) and isset($_GET["score"]) and isset($_GET["chapter"]) and isset($_GET["character"]) and isset($_GET["hash"])) {
    require "src/postScore.php";
}
elseif(isset($_GET["postUsername"]) and isset($_GET["username"]) and isset($_GET["hash"])) {
    require "src/postUsername.php";
}

$mysqli->close();